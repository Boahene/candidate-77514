package pages;

import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.pages.PageObject;

public class HomeOffice_Reason extends PageObject {
	
	
	public void selectReason()
	{
		$(By.cssSelector("#response-2"));
		$(By.cssSelector("#response-0"));
		
	}
	
	public void clickOnNextStepButton()
	{
		$(By.cssSelector("#current-question > button")).click();
		
	}

}
