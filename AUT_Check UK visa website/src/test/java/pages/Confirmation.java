package pages;

import net.thucydides.core.annotations.Step;

public class Confirmation {
	
	HomeOffice_Confirmation confirmation;
	
	@Step
	public void checkIfYouNeedAUKVisa()
	{
		confirmation.confirmation();
	}
	
}
