package pages;


import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.pages.PageObject;


public class HomeOffice_UKVisaPage extends PageObject {
	
	public void selectCountry()
	{
		$(By.cssSelector("#response > option:nth-child(100)"));
		$(By.cssSelector("#response > option:nth-child(162)"));
		
	}
	
	public void clickOnNextStepButton()
	{
		$(By.cssSelector("#current-question > button")).click();
		
	}
	

}
