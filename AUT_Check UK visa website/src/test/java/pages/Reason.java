package pages;

import net.thucydides.core.annotations.Step;

public class Reason {
	
	
	HomeOffice_Reason reasons;
	
	@Step
	public void whatAreYouComingToTheUKToDo()
	{
		reasons.selectReason();	
	}
	
	@Step
	public void clickOnNextButton()
	{
		reasons.clickOnNextStepButton();
	}

}
