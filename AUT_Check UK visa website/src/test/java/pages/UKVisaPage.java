package pages;

import net.thucydides.core.annotations.Step;

public class UKVisaPage {
	
	
	HomeOffice_UKVisaPage home;
	
	
	@Step
	public void openPage()
	{
		home.open();
	}
		
	@Step
	public void checkIfYouNeedAUKVisa()
	{
		home.selectCountry();
	}
	
	@Step
	public void clickOnNextButton()
	{
		home.clickOnNextStepButton();
	}
	

}
