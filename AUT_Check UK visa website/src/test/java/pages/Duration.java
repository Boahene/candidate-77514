package pages;

import net.thucydides.core.annotations.Step;

public class Duration {
	
	
	HomeOffice_Duration duration;
	
	
	@Step
	public void howLongAreYouPlanningToStudyInTheUKFor()
	{
		duration.selectDuration();
	}
	
	@Step
	public void clickOnNextButton()
	{
		duration.clickOnNextStepButton();
	}

}






