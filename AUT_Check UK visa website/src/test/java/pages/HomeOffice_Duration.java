package pages;

import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.pages.PageObject;

public class HomeOffice_Duration extends PageObject {
	
	
	public void selectDuration()
	{
		$(By.id("response-1")).click();
		
	}
	
	public void clickOnNextStepButton()
	{
		$(By.cssSelector("#current-question > button")).click();
		
	}

}
