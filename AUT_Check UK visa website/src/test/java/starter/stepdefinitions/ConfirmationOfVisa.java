package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.Confirmation;
import pages.Duration;
import pages.Reason;
import pages.UKVisaPage;

public class ConfirmationOfVisa {
	
	@Steps
	UKVisaPage home;
	
	@Steps
	Reason reasons;
	
	@Steps
	Duration duration;
	
	@Steps
	Confirmation confirmationPage;
	
	
	@Given("I am on the UK HomeOffice page")
	public void i_am_on_the_UK_HomeOffice_page() {
	    home.openPage();
	}   
	
	@And("I provide a nationality of Japan")
	public void i_provide_a_nationality_of_Japan() {
		home.checkIfYouNeedAUKVisa();
		home.clickOnNextButton();  
	}
	@And("I provide a nationality of Russia")
	public void i_provide_a_nationality_of_Russia() {
		home.checkIfYouNeedAUKVisa();
		home.clickOnNextButton(); 
	}
	
	@Given("I select the reason {string}")
	public void i_select_the_reason(String string) {
		reasons.whatAreYouComingToTheUKToDo();
		reasons.clickOnNextButton();	
	}
	    
	@And("I state I am intending to stay for more than {int} months")
	public void i_state_I_am_intending_to_stay_for_more_than_months(Integer int1) {
	   duration.howLongAreYouPlanningToStudyInTheUKFor();   
	}
	@And("I state I am not travelling or visiting a partner or family")
	public void i_state_I_am_not_travelling_or_visiting_a_partner_or_family() {
		duration.howLongAreYouPlanningToStudyInTheUKFor();
	}
	

	@When("I submit the form")
	public void i_submit_the_form() {
		duration.clickOnNextButton();
	   
	}

	@Then("I will be informed “I need a visa to study in the UK”")
	public void i_will_be_informed_I_need_a_visa_to_study_in_the_UK() {
		confirmationPage.checkIfYouNeedAUKVisa();
	}
	@Then("I will be informed “I need a visa to come to the UK”")
	public void i_will_be_informed_I_need_a_visa_to_come_to_the_UK() {
		confirmationPage.checkIfYouNeedAUKVisa();
	}

}
